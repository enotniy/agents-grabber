<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Routing\Controller;

class PageController extends Controller
{

    public function about(){

        $name = "Kitselyuk <strong>Egor</strong>";

        return view('pages.about')->with('name', $name);
    }

    

}
