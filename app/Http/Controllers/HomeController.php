<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\View;


class HomeController extends BaseController{


    public function showHome() {
        return View::make('welcome');
    }

    public function start(){

        /*
          Output A spreadsheet which has the following columns
            Agent name,
            Agent profile URL,
            Agent Claimed (yes / no),
            Property URL,
            Property Status (Sold / Active),
            Property Address,
            Background Image (Default / Custom)
            sold price,
            sold date
            number of bed,
            bath
            car
         */

        $property_count = 10000;
        $delay = 15;
        $skip = 0;
        $agents_count = 1000 - $skip;

        $background_default = "https://cdn.ratemyagent.com.au/default-listing-icon.png";

        $url_site = "https://www.ratemyagent.com.au";
        $url_site_api = "https://api.ratemyagent.com.au/";
        $url_search = "{$url_site_api}/Agent/AgentSearch?AgentName=&PageData.OrderBy=TOTAL_SALES_QTY&PageData.Skip={$skip}&PageData.Take={$agents_count}&RegionId=";
        $url_property = "{$url_site_api}/PropertyCampaigns?Skip=0&Take={$property_count}&AgentCode=";
        $url_agent = "{$url_site_api}/Agents/Code-";


        $json_search = file_get_contents($url_search);
        $obj =  json_decode($json_search);
        sleep($delay);

        $counter_property = $skip;
        $counter_agent = 0;

        file_put_contents("res.csv", "N;AgentName;AgentProfileURL;AgentIsClaimed;PropertyUrl;PropertyStatus;PropertyAdsress;PropertyBackgroundImage;PropertyPrice;PropertySaleDate;PropertyBathrooms;PropertyBedrooms;PropertyCarparks;PropertyTitle;PropertyDescription\n" , FILE_APPEND);


        foreach ($obj->Data as $user) {
            $counter_property++;
            $tmp = explode("-", $user->Stub);
            $code = $tmp[count($tmp) - 1];
            $json_properties = file_get_contents($url_property . $code);
            $obj_prop = json_decode($json_properties);
            sleep($delay);

            $json_agent = file_get_contents($url_agent . $code);
            $obj_agent = json_decode($json_agent);
            sleep($delay);

            if ($obj_prop != null && $user != null) {
                foreach ($obj_prop->Results as $property) {
                    $agent = [
                        "AgentNumber" => $counter_property,
                        "AgentName" => $user->Name,
                        "AgentProfileURL" => $url_site . $user->AgentProfileUrl,
                        "AgentIsClaimed" => $obj_agent->About->IsClaimed ? "YES" : "NO",
                        "PropertyUrl" => $url_site . $property->PropertyUrl,
                        "PropertyStatus" => $property->Status,
                        "PropertyAdsress" => $property->StreetAddress . ", " . $property->Suburb . ", " . $property->Postcode,
                        "PropertyBackgroundImage" => property_exists($property, "PropertyCoverImage") && $property->PropertyCoverImage != $background_default ? "Custom" : "Default",
                        "PropertyPrice" => property_exists($property, "Price") && $property->Price != null ? $property->Price : "",
                        "PropertySaleDate" => property_exists($property, "SaleDate") && $property->SaleDate != null ? date_format(date_create($property->SaleDate), "d/m/Y") : "",
                        "PropertyBathrooms" => $property->Bathrooms,
                        "PropertyBedrooms" => $property->Bedrooms,
                        "PropertyCarparks" => $property->Carparks,
                        "PropertyTitle" => property_exists($property, "Title") && $property->Title != null ? htmlspecialchars($property->Title) : "",
                        "PropertyDescription" => property_exists($property, "Description") && $property->Description != null ? htmlspecialchars(trim($property->Description)) : "",
                    ];
                    $counter_agent++;
                    file_put_contents("res.csv", '"' . implode('";"', $agent) . '"' . "\n", FILE_APPEND);
                }
            }
        }


        return "Done!<br/>Agents: {$counter_property}<br/>Properties:{$counter_agent} <a href='res.csv'>File</a>";
    }
}
