<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>

    $(document).ready(function(){

        $("#start").click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':  $( '#form input[name=_token]').val()
                }
            })

            e.preventDefault();

            $('#msg').html('loading...')

            $.ajax({
                type: "POST",
                url: "/",
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    $('#msg').html(data.responseText)
                },
                error: function (data) {
                    console.log(data);
                    $('#msg').html(data.responseText)
                }
            });
        });
    });
</script>

</head>
<body>
<h1 id="msg">None</h1>
{!! Form::open(["id" => "form"]) !!}
{!! Form::submit('Click Me!', [ "id" => "start"]) !!}
{!! Form::close() !!}
</body>

<a href='res.csv'>Old file</a>
</html>